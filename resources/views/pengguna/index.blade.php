@extends('layouts.app')

@section('content')
    <div class="container mb-5">
        <div class="card mb-2">
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <div>Pengguna</div>
                    <a href="{{ route('pengguna.create') }}" class="btn btn-primary btn-sm">Tambah</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $key)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>{{ $key->name }}</td>
                                <td>{{ $key->email }}</td>
                                <td>
                                    <form action="{{ route('pengguna.destroy', $key->id) }}" method="POST"
                                        class="text-center">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('pengguna.edit', $key->id) }}"
                                            class="btn btn-outline-primary btn-sm">Edit</a>
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
