@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <img src="{{ asset('assets/images/poster.jpg') }}" alt="poster" class="w-100">
            </div>
            <div class="col-md-5">
                <div class="row mb-4">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="fw-bold h3">
                                    {{ $kriteria->count() }}
                                </div>
                                <div class="text-muted">
                                    Kriteria
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="fw-bold h3">
                                    {{ $alternatif->count() }}
                                </div>
                                <div class="text-muted">
                                    Alternatif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-header">Ranking Pemberkasan</div>
                    <div class="card-body" style="max-height: 400px; overflow: auto">
                        <table class="table-striped table-bordered table">
                            <thead>
                                <tr>
                                    <th style="width: 10%" class="text-center">#</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alternatif as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            {{ $item->nama }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Ranking Terkini</div>
                    <div class="card-body" style="max-height: 400px; overflow: auto">
                        <table class="table-striped table-bordered table">
                            <thead>
                                <tr>
                                    <th style="width: 10%" class="text-center">#</th>
                                    <th>Nama</th>
                                    <th style="width: 20%" class="text-end">Score</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($alternatif as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            {{ $item->nama }}
                                        </td>
                                        <td class="text-end">
                                            {{ $item->nilai_total ?? '-' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
