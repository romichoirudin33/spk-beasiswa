<h5 class="fw-bold">PERHITUNGAN SAW</h5>
<div class="row mb-3">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">1. PENENTUAN KRITERIA</div>
            <div class="card-body">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2">Nama</th>
                            <th colspan="2">Kriteria</th>
                        </tr>
                        <tr>
                            <th>Pemberkasan</th>
                            <th>Wawancara</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pemberkasanWawancara as $key)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['pemberkasan'] }}</td>
                                <td>{{ $key['wawancara'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot class="fw-bold">
                        <tr>
                            <td colspan="2">Nilai Max</td>
                            <td>{{ $nilaiMinMax['max_pemberkasan'] }}</td>
                            <td>{{ $nilaiMinMax['max_wawancara'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Nilai MIN</td>
                            <td>{{ $nilaiMinMax['min_pemberkasan'] }}</td>
                            <td>{{ $nilaiMinMax['min_wawancara'] }}</td>
                        </tr>
                    </tfoot>
                </table>

                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="2">BOBOT MASING-MASING KRITERIA</th>
                        </tr>
                        <tr>
                            <th>PEMBERKASAN</th>
                            <th>WAWANCARA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>0.2</td>
                            <td>0.8</td>
                        </tr>
                    </tbody>
                </table>

                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="6">KETENTUAN NORMALISASI</th>
                        </tr>
                        <tr>
                            <th>KODE</th>
                            <th>KRITERIA</th>
                            <th>JENIS KRITERIA</th>
                            <th>KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>SAW1</td>
                            <td>PEMBERKASAN</td>
                            <td>KEUNTUNGAN</td>
                            <td>Makin besar nilai makin baik</td>
                        </tr>
                        <tr>
                            <td>SAW2</td>
                            <td>WAWANCARA</td>
                            <td>KEUNTUNGAN</td>
                            <td>Makin besar nilai makin baik</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">2. NORMALISASI</div>
            <div class="card-body">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="6">HASIL NORMALISASI</th>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <th>SAW1</th>
                            <th>SAW2</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tableNormalisasi as $key)
                            <tr>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['saw_1'] }}</td>
                                <td>{{ $key['saw_2'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">3. MENGHITUNG NILAI REFERENSI</div>
            <div class="card-body">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>Nama</th>
                            <th>V1</th>
                            <th>V2</th>
                            <th>NILAI TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tableNilaiReferensi as $key)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['v_1'] }}</td>
                                <td>{{ $key['v_2'] }}</td>
                                <td>{{ $key['nilai_total'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
