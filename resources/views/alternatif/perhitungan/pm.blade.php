<h5 class="fw-bold">PERHITUNGAN PM</h5>
<div class="row mb-3">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">KRITERIA PEMBERKASAN</div>
            <div class="card-body">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="11">A. Nilai Aspek Pemberkasan</th>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <th>P1</th>
                            <th>P2</th>
                            <th>P3</th>
                            <th>P4</th>
                            <th>P5</th>
                            <th>P6</th>
                            <th>P7</th>
                            <th>P8</th>
                            <th>P9</th>
                            <th>P10</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($nilaiAspekPemberkasan as $key)
                            <tr>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['P1'] }}</td>
                                <td>{{ $key['P2'] }}</td>
                                <td>{{ $key['P3'] }}</td>
                                <td>{{ $key['P4'] }}</td>
                                <td>{{ $key['P5'] }}</td>
                                <td>{{ $key['P6'] }}</td>
                                <td>{{ $key['P7'] }}</td>
                                <td>{{ $key['P8'] }}</td>
                                <td>{{ $key['P9'] }}</td>
                                <td>{{ $key['P10'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="11">Table Perhitungan GAP Pemberkasan</th>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <th>P1</th>
                            <th>P2</th>
                            <th>P3</th>
                            <th>P4</th>
                            <th>P5</th>
                            <th>P6</th>
                            <th>P7</th>
                            <th>P8</th>
                            <th>P9</th>
                            <th>P10</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tablePerhitunganGabPemberkasan as $key)
                            <tr>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['P1'] }}</td>
                                <td>{{ $key['P2'] }}</td>
                                <td>{{ $key['P3'] }}</td>
                                <td>{{ $key['P4'] }}</td>
                                <td>{{ $key['P5'] }}</td>
                                <td>{{ $key['P6'] }}</td>
                                <td>{{ $key['P7'] }}</td>
                                <td>{{ $key['P8'] }}</td>
                                <td>{{ $key['P9'] }}</td>
                                <td>{{ $key['P10'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">KRITERIA WAWANCARA</div>
            <div class="card-body">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="6">B. Nilai Aspek Wawancara</th>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <th>W1</th>
                            <th>W2</th>
                            <th>W3</th>
                            <th>W4</th>
                            <th>W5</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($nilaiAspekWawancara as $key)
                            <tr>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['W1'] }}</td>
                                <td>{{ $key['W2'] }}</td>
                                <td>{{ $key['W3'] }}</td>
                                <td>{{ $key['W4'] }}</td>
                                <td>{{ $key['W5'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="6">Tabel Perhitungan GAP Wawancara</th>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <th>W1</th>
                            <th>W2</th>
                            <th>W3</th>
                            <th>W4</th>
                            <th>W5</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tablePerhitunganGabWawancara as $key)
                            <tr>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['W1'] }}</td>
                                <td>{{ $key['W2'] }}</td>
                                <td>{{ $key['W3'] }}</td>
                                <td>{{ $key['W4'] }}</td>
                                <td>{{ $key['W5'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="card mb-3">
    <div class="card-header">Tabel Bobot Nilai</div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8 table-responsive">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="14">Tabel Bobot Nilai Pemberkasan</th>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <th>P1</th>
                            <th>P2</th>
                            <th>P3</th>
                            <th>P4</th>
                            <th>P5</th>
                            <th>P6</th>
                            <th>P7</th>
                            <th>P8</th>
                            <th>P9</th>
                            <th>P10</th>
                            <th>NCF</th>
                            <th>NSF</th>
                            <th>NT</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tableBobotNilaiPemberkasan as $key)
                            <tr>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['P1'] }}</td>
                                <td>{{ $key['P2'] }}</td>
                                <td>{{ $key['P3'] }}</td>
                                <td>{{ $key['P4'] }}</td>
                                <td>{{ $key['P5'] }}</td>
                                <td>{{ $key['P6'] }}</td>
                                <td>{{ $key['P7'] }}</td>
                                <td>{{ $key['P8'] }}</td>
                                <td>{{ $key['P9'] }}</td>
                                <td>{{ $key['P10'] }}</td>
                                <td>{{ $key['NCF'] }}</td>
                                <td>{{ $key['NSF'] }}</td>
                                <td>{{ $key['NT'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="9">Tabel Bobot Nilai Wawancara</th>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <th>W1</th>
                            <th>W2</th>
                            <th>W3</th>
                            <th>W4</th>
                            <th>W5</th>
                            <th>NCF</th>
                            <th>NCF</th>
                            <th>NT</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tableBobotNilaiWawancara as $key)
                            <tr>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['W1'] }}</td>
                                <td>{{ $key['W2'] }}</td>
                                <td>{{ $key['W3'] }}</td>
                                <td>{{ $key['W4'] }}</td>
                                <td>{{ $key['W5'] }}</td>
                                <td>{{ $key['NCF'] }}</td>
                                <td>{{ $key['NSF'] }}</td>
                                <td>{{ $key['NT'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-4 table-responsive">
                <table class="table-bordered table">
                    <thead>
                        <tr>
                            <th colspan="3">Pemberkasan dan Wawancara</th>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <th>Pemberkasan</th>
                            <th>Wawancara</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pemberkasanWawancara as $key)
                            <tr>
                                <td>{{ $key['nama'] }}</td>
                                <td>{{ $key['pemberkasan'] }}</td>
                                <td>{{ $key['wawancara'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
