@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{ route('alternatif.update', $alternatif->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-header">Ubah Data Alternatif</div>
                        <div class="card-body">
                            <div class="form-group mb-2">
                                <label for="nama">Nama</label>
                                <input type="text" name="nama" class="form-control" value="{{ $alternatif->nama }}"
                                    required>
                            </div>
                            @foreach ($kriteria as $item)
                                <div class="form-group mb-2">
                                    <label for="nama" class="fw-bold">{{ $item->nama }}</label>
                                    @foreach ($item->sub_kriteria as $key)
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"
                                                name="kriteria[{{ $item->id }}]" value="{{ $key->id }}"
                                                id="sub_kriteria_id_{{ $key->id }}"
                                                {{ $alternatif->details()->where('sub_kriteria_id', $key->id)->first()? 'checked': '' }}>
                                            <label class="form-check-label" for="sub_kriteria_id_{{ $key->id }}">
                                                {{ $key->nama }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
