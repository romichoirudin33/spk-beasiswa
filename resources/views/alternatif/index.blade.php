@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>Alternatif</div>
                    <div>
                        <a href="{{ route('alternatif.perhitungan') }}" class="btn btn-outline-primary btn-sm">Proses
                            Perhitungan</a>
                        <a href="{{ route('alternatif.create') }}" class="btn btn-primary btn-sm">Tambah</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table-bordered table">
                    <thead class="table-primary fw-bold">
                        <tr>
                            <td width='10%' class="text-center">#</td>
                            <td>Nama</td>
                            <td>Jawaban</td>
                            <td class="text-center">Total Nilai</td>
                            <td class="text-center">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($alternatif as $item)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>{{ $item['nama'] }}</td>
                                <td>
                                    @foreach ($item->details as $detail)
                                        <div class="mb-2">
                                            <div class="fw-bold">{{ $detail->sub_kriteria->kriteria->nama }}</div>
                                            {{ $detail->sub_kriteria->nama }}
                                        </div>
                                    @endforeach
                                </td>
                                <td class="text-center">
                                    @if ($item->nilai_total)
                                        {{ $item->nilai_total }}
                                    @else
                                        <a href="{{ route('alternatif.perhitungan', ['redirect' => true]) }}"
                                            class="btn btn-outline-primary btn-sm">
                                            Lakukan perhitungan
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <form action="{{ route('alternatif.destroy', $item->id) }}" method="POST"
                                        onsubmit="return confirm('Anda yakin akan menghapus ini?')" class="text-center">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('alternatif.edit', $item->id) }}"
                                            class="btn btn-outline-primary btn-sm">Edit</a>
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
