@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>Rangking Alternatif</div>
                </div>
            </div>
            <div class="card-body">
                <table class="table-bordered table">
                    <thead class="table-primary fw-bold">
                        <tr>
                            <td width='10%' class="text-center">Peringkat</td>
                            <td>Nama</td>
                            <td width='15%' class="text-center">Total Nilai</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($alternatif as $item)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>{{ $item['nama'] }}</td>
                                <td class="text-center">
                                    @if ($item->nilai_total)
                                        {{ $item->nilai_total }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
