<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>

<body>
    <div id="app" class="my-content">
        <nav class="navbar navbar-expand-md navbar-dark sticky-top custom-navbar">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{-- {{ config('app.name', 'Laravel') }} --}}
                    <img src="{{ asset('assets/images/logo-bank-indonesia-putih.png') }}" alt="logo-bi"
                        style="max-height: 40px">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    @auth
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav me-auto">
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('kriteria*') ? 'active' : '' }}"
                                    href="{{ route('kriteria.index') }}">Kriteria</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('alternatif*') ? 'active' : '' }}"
                                    href="{{ route('alternatif.index') }}">Alternatif</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('rangking*') ? 'active' : '' }}"
                                    href="{{ route('rangking') }}">Rangking</a>
                            </li>
                        </ul>
                    @endauth

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link active dropdown-toggle" href="#" role="button"
                                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a href="{{ route('pengguna.index') }}" class="dropdown-item">Pengguna</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <footer>
        <div class="custom-pre-footer">
            <div class="container">
                <div class="d-flex justify-content-between align-items-end">
                    <div>
                        <div class="fw-bold h5">
                            Bank Indonesia
                        </div>
                        <p class="text-muted h6 mb-1">
                            Jalan M.H. Thamrin No. 2, Jakarta 10350<br>
                            Contact Center Bank Indonesia Bicara<br>
                            Telp. : 131 dan 1500131 (dari luar negeri)<br>
                        </p>
                    </div>
                    <p class="text-muted h6 mb-1">
                        E-mail : <a href="mailto:bicara@bi.go.id">bicara@bi.go.id</a><br>
                        Chatbot LISA :<a href="https://wa.me/6281131131131?text=Hi,%20Lisa" target="blank">
                            081 131 131 131</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="custom-footer">
            <div class="container text-end text-white">
                @ 2024 Bank Indonesia
            </div>
        </div>
    </footer>
</body>

</html>
