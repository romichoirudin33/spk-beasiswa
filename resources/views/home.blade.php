@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <p>
                            Selamat datang, Aplikasi ini untuk menerapkan Sistem Pendukung Keputusan dengan metode PM
                            (Profile Matching) dan SAW (Simple Additive Weighting) dalam menentukan penerima beasiswa.
                        </p>
                        <p>
                            Dalam metode PM, terdapat beberapa kriteria yang telah di simpan pada database. Kriteria
                            tersebut meliputi Pemberkasan dan Wawancara yang di asumsikan dengan beberapa pertanyaan.
                        </p>
                        <p>Sedangkan hasil dari pembobotan Kriteria Pemberkasan dan Wawancara yang telah dihitung
                            menggunakan PM, akan dijadikan sebagai penentuan kriteria menggunakan SAW. Hasil dari
                            perhitungan SAW akan di jadikan total nilai yang dimiliki oleh alternatif.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
