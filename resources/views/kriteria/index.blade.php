@extends('layouts.app')

@section('content')
    <div class="container mb-5">
        <h5 class="fw-bold mb-3">Kriteria</h5>
        @foreach ($data as $key)
            <div class="card mb-2">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <div>
                            {{ $key->kode }} - {{ $key->nama }}
                        </div>
                        <div class="text-end">
                            <div>{{ $key->tahap }}</div>
                            {{ $key->jenis_kriteria->nama }}
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <p>
                        Jawaban terkait kriteria ini ?
                    </p>
                    @if ($key->sub_kriteria)
                        <ol type='A'>
                            @foreach ($key->sub_kriteria as $item)
                                <li>{{ $item->nama }} - <div class="fw-bold d-inline-block">Bobot ({{ $item->nilai }})
                                    </div>
                                </li>
                            @endforeach
                        </ol>
                    @else
                        <div>Tidak terdapat jawaban untuk kriteria ini !</div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
@endsection
