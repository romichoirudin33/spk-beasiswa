<?php

namespace Database\Seeders;

use App\Models\Alternatif;
use App\Models\Kriteria;
use App\Models\SubKriteria;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlternatifSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'nama' => 'PUTU',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 4],
                    ['kriteria_kode' => 'P2', 'nilai' => 4],
                    ['kriteria_kode' => 'P3', 'nilai' => 4],
                    ['kriteria_kode' => 'P4', 'nilai' => 4],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 5],
                    ['kriteria_kode' => 'W2', 'nilai' => 5],
                    ['kriteria_kode' => 'W3', 'nilai' => 5],
                    ['kriteria_kode' => 'W4', 'nilai' => 5],
                    ['kriteria_kode' => 'W5', 'nilai' => 5],
                ]
            ],
            [
                'nama' => 'SYINTA',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 4],
                    ['kriteria_kode' => 'P2', 'nilai' => 4],
                    ['kriteria_kode' => 'P3', 'nilai' => 3],
                    ['kriteria_kode' => 'P4', 'nilai' => 3],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 5],
                    ['kriteria_kode' => 'W2', 'nilai' => 4],
                    ['kriteria_kode' => 'W3', 'nilai' => 4],
                    ['kriteria_kode' => 'W4', 'nilai' => 4],
                    ['kriteria_kode' => 'W5', 'nilai' => 5],
                ]
            ],
            [
                'nama' => 'JODDY',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 3],
                    ['kriteria_kode' => 'P2', 'nilai' => 4],
                    ['kriteria_kode' => 'P3', 'nilai' => 3],
                    ['kriteria_kode' => 'P4', 'nilai' => 2],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 5],
                    ['kriteria_kode' => 'W2', 'nilai' => 3],
                    ['kriteria_kode' => 'W3', 'nilai' => 4],
                    ['kriteria_kode' => 'W4', 'nilai' => 3],
                    ['kriteria_kode' => 'W5', 'nilai' => 4],
                ]
            ],
            [
                'nama' => 'SATYA',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 2],
                    ['kriteria_kode' => 'P2', 'nilai' => 2],
                    ['kriteria_kode' => 'P3', 'nilai' => 4],
                    ['kriteria_kode' => 'P4', 'nilai' => 1],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 2],
                    ['kriteria_kode' => 'W2', 'nilai' => 2],
                    ['kriteria_kode' => 'W3', 'nilai' => 3],
                    ['kriteria_kode' => 'W4', 'nilai' => 2],
                    ['kriteria_kode' => 'W5', 'nilai' => 5],
                ]
            ],
            [
                'nama' => 'DITA',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 1],
                    ['kriteria_kode' => 'P2', 'nilai' => 3],
                    ['kriteria_kode' => 'P3', 'nilai' => 2],
                    ['kriteria_kode' => 'P4', 'nilai' => 3],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 3],
                    ['kriteria_kode' => 'W2', 'nilai' => 1],
                    ['kriteria_kode' => 'W3', 'nilai' => 2],
                    ['kriteria_kode' => 'W4', 'nilai' => 2],
                    ['kriteria_kode' => 'W5', 'nilai' => 3],
                ]
            ],
            [
                'nama' => 'LILIK',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 2],
                    ['kriteria_kode' => 'P2', 'nilai' => 4],
                    ['kriteria_kode' => 'P3', 'nilai' => 3],
                    ['kriteria_kode' => 'P4', 'nilai' => 2],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 4],
                    ['kriteria_kode' => 'W2', 'nilai' => 2],
                    ['kriteria_kode' => 'W3', 'nilai' => 1],
                    ['kriteria_kode' => 'W4', 'nilai' => 3],
                    ['kriteria_kode' => 'W5', 'nilai' => 2],
                ]
            ],
            [
                'nama' => 'NYOMAN',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 3],
                    ['kriteria_kode' => 'P2', 'nilai' => 2],
                    ['kriteria_kode' => 'P3', 'nilai' => 1],
                    ['kriteria_kode' => 'P4', 'nilai' => 4],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 1],
                    ['kriteria_kode' => 'W2', 'nilai' => 3],
                    ['kriteria_kode' => 'W3', 'nilai' => 3],
                    ['kriteria_kode' => 'W4', 'nilai' => 4],
                    ['kriteria_kode' => 'W5', 'nilai' => 1],
                ]
            ],
            [
                'nama' => 'KETUT',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 1],
                    ['kriteria_kode' => 'P2', 'nilai' => 3],
                    ['kriteria_kode' => 'P3', 'nilai' => 2],
                    ['kriteria_kode' => 'P4', 'nilai' => 2],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 2],
                    ['kriteria_kode' => 'W2', 'nilai' => 4],
                    ['kriteria_kode' => 'W3', 'nilai' => 4],
                    ['kriteria_kode' => 'W4', 'nilai' => 5],
                    ['kriteria_kode' => 'W5', 'nilai' => 3],
                ]
            ],
            [
                'nama' => 'PUTU',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 2],
                    ['kriteria_kode' => 'P2', 'nilai' => 2],
                    ['kriteria_kode' => 'P3', 'nilai' => 3],
                    ['kriteria_kode' => 'P4', 'nilai' => 3],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 3],
                    ['kriteria_kode' => 'W2', 'nilai' => 3],
                    ['kriteria_kode' => 'W3', 'nilai' => 5],
                    ['kriteria_kode' => 'W4', 'nilai' => 2],
                    ['kriteria_kode' => 'W5', 'nilai' => 4],
                ]
            ],
            [
                'nama' => 'ADIT',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 3],
                    ['kriteria_kode' => 'P2', 'nilai' => 3],
                    ['kriteria_kode' => 'P3', 'nilai' => 4],
                    ['kriteria_kode' => 'P4', 'nilai' => 3],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 4],
                    ['kriteria_kode' => 'W2', 'nilai' => 2],
                    ['kriteria_kode' => 'W3', 'nilai' => 3],
                    ['kriteria_kode' => 'W4', 'nilai' => 3],
                    ['kriteria_kode' => 'W5', 'nilai' => 5],
                ]
            ],
            [
                'nama' => 'FIRA',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 4],
                    ['kriteria_kode' => 'P2', 'nilai' => 4],
                    ['kriteria_kode' => 'P3', 'nilai' => 2],
                    ['kriteria_kode' => 'P4', 'nilai' => 2],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 5],
                    ['kriteria_kode' => 'W2', 'nilai' => 4],
                    ['kriteria_kode' => 'W3', 'nilai' => 2],
                    ['kriteria_kode' => 'W4', 'nilai' => 1],
                    ['kriteria_kode' => 'W5', 'nilai' => 3],
                ]
            ],
            [
                'nama' => 'PIPIT',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 4],
                    ['kriteria_kode' => 'P2', 'nilai' => 3],
                    ['kriteria_kode' => 'P3', 'nilai' => 1],
                    ['kriteria_kode' => 'P4', 'nilai' => 2],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 3],
                    ['kriteria_kode' => 'W2', 'nilai' => 5],
                    ['kriteria_kode' => 'W3', 'nilai' => 1],
                    ['kriteria_kode' => 'W4', 'nilai' => 2],
                    ['kriteria_kode' => 'W5', 'nilai' => 2],
                ]
            ],
            [
                'nama' => 'AXEL',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 4],
                    ['kriteria_kode' => 'P2', 'nilai' => 2],
                    ['kriteria_kode' => 'P3', 'nilai' => 3],
                    ['kriteria_kode' => 'P4', 'nilai' => 2],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 4],
                    ['kriteria_kode' => 'W2', 'nilai' => 3],
                    ['kriteria_kode' => 'W3', 'nilai' => 2],
                    ['kriteria_kode' => 'W4', 'nilai' => 3],
                    ['kriteria_kode' => 'W5', 'nilai' => 1],
                ]
            ],
            [
                'nama' => 'ALEX',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 4],
                    ['kriteria_kode' => 'P2', 'nilai' => 3],
                    ['kriteria_kode' => 'P3', 'nilai' => 3],
                    ['kriteria_kode' => 'P4', 'nilai' => 3],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 1],
                    ['kriteria_kode' => 'W2', 'nilai' => 4],
                    ['kriteria_kode' => 'W3', 'nilai' => 3],
                    ['kriteria_kode' => 'W4', 'nilai' => 4],
                    ['kriteria_kode' => 'W5', 'nilai' => 3],
                ]
            ],
            [
                'nama' => 'JORDAN',
                'detail' => [
                    ['kriteria_kode' => 'P1', 'nilai' => 2],
                    ['kriteria_kode' => 'P2', 'nilai' => 4],
                    ['kriteria_kode' => 'P3', 'nilai' => 4],
                    ['kriteria_kode' => 'P4', 'nilai' => 2],
                    ['kriteria_kode' => 'P5', 'nilai' => 4],
                    ['kriteria_kode' => 'P6', 'nilai' => 4],
                    ['kriteria_kode' => 'P7', 'nilai' => 4],
                    ['kriteria_kode' => 'P8', 'nilai' => 4],
                    ['kriteria_kode' => 'P9', 'nilai' => 4],
                    ['kriteria_kode' => 'P10', 'nilai' => 4],
                    ['kriteria_kode' => 'W1', 'nilai' => 2],
                    ['kriteria_kode' => 'W2', 'nilai' => 4],
                    ['kriteria_kode' => 'W3', 'nilai' => 4],
                    ['kriteria_kode' => 'W4', 'nilai' => 5],
                    ['kriteria_kode' => 'W5', 'nilai' => 4],
                ]
            ],
        ];

        DB::transaction(function () use ($data) {
            foreach ($data as $item) {
                $alternatif = Alternatif::query()->create([
                    'nama' => $item['nama']
                ]);

                foreach ($item['detail'] as $detail) {
                    $kriteria = Kriteria::where('kode', $detail['kriteria_kode'])->first();
                    $sub_kriteria = SubKriteria::query()
                        ->where('kriteria_id', $kriteria->id)
                        ->where('nilai', $detail['nilai'])
                        ->first();

                    $alternatif->details()->create([
                        'kriteria_id' => $kriteria->id,
                        'sub_kriteria_id' => $sub_kriteria->id,
                        'nilai' => $detail['nilai']
                    ]);
                }
            }
        });
    }
}
