<?php

namespace Database\Seeders;

use App\Models\PemetaanGap;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'gap' => 0,
                'bobot_nilai' => 5,
                'keterangan' => 'Kompetensi sesuai kebutuhan',
            ],
            [
                'gap' => 1,
                'bobot_nilai' => 4.5,
                'keterangan' => 'Kompetensi kelebihan 1 tingkat/level',
            ],
            [
                'gap' => -1,
                'bobot_nilai' => 4,
                'keterangan' => 'Kompetensi kekurangan 1 tingkat/level',
            ],
            [
                'gap' => 2,
                'bobot_nilai' => 3.5,
                'keterangan' => 'Kompetensi kelebihan 2 tingkat/level',
            ],
            [
                'gap' => -2,
                'bobot_nilai' => 3,
                'keterangan' => 'Kompetensi kekurangan 2 tingkat/level',
            ],
            [
                'gap' => 3,
                'bobot_nilai' => 2.5,
                'keterangan' => 'Kompetensi kelebihan 3 tingkat/level',
            ],
            [
                'gap' => -3,
                'bobot_nilai' => 2,
                'keterangan' => 'Kompetensi kekurangan 3 tingkat/level',
            ],
            [
                'gap' => 4,
                'bobot_nilai' => 1.5,
                'keterangan' => 'Kompetensi kelebihan 4 tingkat/level',
            ],
            [
                'gap' => -4,
                'bobot_nilai' => 1,
                'keterangan' => 'Kompetensi kekurangan 4 tingkat/level',
            ],
        ];

        PemetaanGap::insert($data);
    }
}
