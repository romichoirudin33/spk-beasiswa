<?php

namespace Database\Seeders;

use App\Models\JenisKriteria;
use App\Models\Kriteria;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'id' => 1,
                'nama' => 'Core Factor'
            ],
            [
                'id' => 2,
                'nama' => 'Secondary Factor'
            ],
        ];

        JenisKriteria::insert($data);

        $persen70 = 0.7 / 3;
        $persen30 = 0.3 / 2;

        $persen60 = 0.6 / 3;
        $persen40 = 0.4 / 7;
        $data = [
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P1',
                'nama' => 'Resume',
                'jenis_kriteria_id' => 1,
                'nilai' => $persen60,
                'sub_kriteria' => [
                    [
                        'nama' => 'Kurang Baik',
                        'nilai' => 1
                    ],
                    [
                        'nama' => 'Cukup',
                        'nilai' => 2
                    ],
                    [
                        'nama' => 'Baik',
                        'nilai' => 3
                    ],
                    [
                        'nama' => 'Sangat Baik',
                        'nilai' => 4
                    ]
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P2',
                'nama' => 'Foto copy sertifikat',
                'jenis_kriteria_id' => 1,
                'nilai' => $persen60,
                'sub_kriteria' => [
                    [
                        'nama' => '0',
                        'nilai' => 0
                    ],
                    [
                        'nama' => '1 dan 2',
                        'nilai' => 1
                    ],
                    [
                        'nama' => '2 dan 3',
                        'nilai' => 2
                    ],
                    [
                        'nama' => '4',
                        'nilai' => 3
                    ],
                    [
                        'nama' => '>4',
                        'nilai' => 4
                    ]
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P3',
                'nama' => 'Slip Gaji Orang Tua',
                'jenis_kriteria_id' => 1,
                'nilai' => $persen60,
                'sub_kriteria' => [
                    [
                        'nama' => '>=5.000.000',
                        'nilai' => 1
                    ],
                    [
                        'nama' => '>=3.000.000 dan <=5.000.000',
                        'nilai' => 2
                    ],
                    [
                        'nama' => '>1.500.000 dan <=3.000.000',
                        'nilai' => 3
                    ],
                    [
                        'nama' => '<1.5000.000',
                        'nilai' => 4
                    ]
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P4',
                'nama' => 'IPK',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen40,
                'sub_kriteria' => [
                    [
                        'nama' => '>3 DAN <3,3',
                        'nilai' => 1
                    ],
                    [
                        'nama' => '>3,4 DAN <3,5',
                        'nilai' => 2
                    ],
                    [
                        'nama' => '>3,6 DAN <3,9',
                        'nilai' => 3
                    ],
                    [
                        'nama' => '4',
                        'nilai' => 4
                    ]
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P5',
                'nama' => 'SURAT REKOMENDASI DARI KAMPUS',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen40,
                'sub_kriteria' => [
                    [
                        'nama' => 'TIDAK TERLAMPIR',
                        'nilai' => 0
                    ],
                    [
                        'nama' => 'TERLAMPIR',
                        'nilai' => 4
                    ],
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P6',
                'nama' => 'UMUR',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen40,
                'sub_kriteria' => [
                    [
                        'nama' => '>=24',
                        'nilai' => 0
                    ],
                    [
                        'nama' => '<23',
                        'nilai' => 4
                    ],
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P7',
                'nama' => 'SURAT AKTIF KULIAH',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen40,
                'sub_kriteria' => [
                    [
                        'nama' => 'TIDAK TERLAMPIR',
                        'nilai' => 0
                    ],
                    [
                        'nama' => 'TERLAMPIR',
                        'nilai' => 4
                    ],
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P8',
                'nama' => 'SURAT PERNYATAAN BERSEDIA BERPERAN AKTIF',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen40,
                'sub_kriteria' => [
                    [
                        'nama' => 'TIDAK TERLAMPIR',
                        'nilai' => 0
                    ],
                    [
                        'nama' => 'TERLAMPIR',
                        'nilai' => 4
                    ],
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P9',
                'nama' => 'SURAT PERNYATAAN TIDAK MENERIMA BEASISWA',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen40,
                'sub_kriteria' => [
                    [
                        'nama' => 'TIDAK TERLAMPIR',
                        'nilai' => 0
                    ],
                    [
                        'nama' => 'TERLAMPIR',
                        'nilai' => 4
                    ],
                ],
            ],
            [
                'tahap' => 'PEMBERKASAN',
                'kode' => 'P10',
                'nama' => 'KONTEN EDUKASI DAN BUKTI TRANSAKSI QRIS',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen40,
                'sub_kriteria' => [
                    [
                        'nama' => 'TIDAK TERLAMPIR',
                        'nilai' => 0
                    ],
                    [
                        'nama' => 'TERLAMPIR',
                        'nilai' => 4
                    ],
                ],
            ],
            [
                'tahap' => 'WAWANCARA',
                'kode' => 'W1',
                'nama' => 'Pengetahuan ke Bank Sentralan',
                'jenis_kriteria_id' => 1,
                'nilai' => $persen70,
                'sub_kriteria' => [
                    [
                        'nama' => 'Sangat Rendah',
                        'nilai' => 1
                    ],
                    [
                        'nama' => 'Rendah',
                        'nilai' => 2
                    ],
                    [
                        'nama' => 'Cukup',
                        'nilai' => 3
                    ],
                    [
                        'nama' => 'Tinggi',
                        'nilai' => 4
                    ],
                    [
                        'nama' => 'Sangat Tinggi',
                        'nilai' => 5
                    ]
                ],
            ],
            [
                'tahap' => 'WAWANCARA',
                'kode' => 'W2',
                'nama' => 'Pengetahuan tentang beasiswa BI dan GenBI',
                'jenis_kriteria_id' => 1,
                'nilai' => $persen70,
                'sub_kriteria' => [
                    [
                        'nama' => 'Sangat Rendah',
                        'nilai' => 1
                    ],
                    [
                        'nama' => 'Rendah',
                        'nilai' => 2
                    ],
                    [
                        'nama' => 'Cukup',
                        'nilai' => 3
                    ],
                    [
                        'nama' => 'Tinggi',
                        'nilai' => 4
                    ],
                    [
                        'nama' => 'Sangat Tinggi',
                        'nilai' => 5
                    ]
                ],
            ],
            [
                'tahap' => 'WAWANCARA',
                'kode' => 'W3',
                'nama' => 'Organisasi sosial dan event',
                'jenis_kriteria_id' => 1,
                'nilai' => $persen70,
                'sub_kriteria' => [
                    [
                        'nama' => 'Sangat Rendah',
                        'nilai' => 1
                    ],
                    [
                        'nama' => 'Rendah',
                        'nilai' => 2
                    ],
                    [
                        'nama' => 'Cukup',
                        'nilai' => 3
                    ],
                    [
                        'nama' => 'Tinggi',
                        'nilai' => 4
                    ],
                    [
                        'nama' => 'Sangat Tinggi',
                        'nilai' => 5
                    ]
                ],
            ],
            [
                'tahap' => 'WAWANCARA',
                'kode' => 'W4',
                'nama' => 'Karya ilmiah dan prestasi',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen30,
                'sub_kriteria' => [
                    [
                        'nama' => 'Sangat Rendah',
                        'nilai' => 1
                    ],
                    [
                        'nama' => 'Rendah',
                        'nilai' => 2
                    ],
                    [
                        'nama' => 'Cukup',
                        'nilai' => 3
                    ],
                    [
                        'nama' => 'Tinggi',
                        'nilai' => 4
                    ],
                    [
                        'nama' => 'Sangat Tinggi',
                        'nilai' => 5
                    ]
                ],
            ],
            [
                'tahap' => 'WAWANCARA',
                'kode' => 'W5',
                'nama' => 'Motivasi dan kepribadian',
                'jenis_kriteria_id' => 2,
                'nilai' => $persen30,
                'sub_kriteria' => [
                    [
                        'nama' => 'Sangat Rendah',
                        'nilai' => 1
                    ],
                    [
                        'nama' => 'Rendah',
                        'nilai' => 2
                    ],
                    [
                        'nama' => 'Cukup',
                        'nilai' => 3
                    ],
                    [
                        'nama' => 'Tinggi',
                        'nilai' => 4
                    ],
                    [
                        'nama' => 'Sangat Tinggi',
                        'nilai' => 5
                    ]
                ],
            ],
        ];

        DB::transaction(function () use ($data) {
            foreach ($data as $item) {
                $kriteria = Kriteria::query()->create([
                    'tahap' => $item['tahap'],
                    'kode' => $item['kode'],
                    'nama' => $item['nama'],
                    'jenis_kriteria_id' => $item['jenis_kriteria_id'],
                    'nilai' => $item['nilai'],
                ]);

                foreach ($item['sub_kriteria'] as $sub) {
                    $kriteria->sub_kriteria()->create([
                        'nama' => $sub['nama'],
                        'nilai' => $sub['nilai'],
                    ]);
                }
            }
        });
    }
}
