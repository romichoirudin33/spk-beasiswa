<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SubKriteria extends Model
{
    public $table = 'sub_kriteria';
    public $fillable = [
        'nama',
        'kriteria_id',
        'nilai',
    ];
    public $timestamps = false;

    public function kriteria(): BelongsTo
    {
        return $this->belongsTo(Kriteria::class);
    }
}
