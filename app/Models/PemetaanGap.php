<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PemetaanGap extends Model
{
    public $table = 'pemetaan_gap';
    public $fillable = [
        'gap',
        'bobot_nilai',
        'keterangan',
    ];

    public $timestamps = false;
}
