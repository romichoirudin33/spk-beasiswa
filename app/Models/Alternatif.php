<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Alternatif extends Model
{
    public $table = 'alternatif';
    public $fillable = [
        'nama',
        'nilai_total',
        'pemberkasan_ncf',
        'pemberkasan_nsf',
        'pemberkasan_nt'
    ];

    public function details(): HasMany
    {
        return $this->hasMany(AlternatifDetail::class);
    }
}
