<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Kriteria extends Model
{

    public $table = 'kriteria';
    public $fillable = [
        'tahap',
        'kode',
        'nama',
        'jenis_kriteria_id',
        'nilai',
    ];

    public $timestamps = false;

    public function jenis_kriteria(): BelongsTo
    {
        return $this->belongsTo(JenisKriteria::class);
    }

    public function sub_kriteria(): HasMany
    {
        return $this->hasMany(SubKriteria::class);
    }
}
