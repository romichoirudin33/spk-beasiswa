<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AlternatifDetail extends Model
{
    public $table = 'alternatif_detail';
    public $fillable = [
        'alternatif_id',
        'kriteria_id',
        'sub_kriteria_id',
        'nilai',
    ];
    public $timestamps = false;

    public function alternatif(): BelongsTo
    {
        return $this->belongsTo(Alternatif::class);
    }

    public function kriteria(): BelongsTo
    {
        return $this->belongsTo(Kriteria::class);
    }

    public function sub_kriteria(): BelongsTo
    {
        return $this->belongsTo(SubKriteria::class);
    }
}
