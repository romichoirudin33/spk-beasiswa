<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class JenisKriteria extends Model
{
    public $table = 'jenis_kriteria';
    public $fillable = [
        'nama'
    ];

    public $timestamps = false;

    public function kriteria(): HasMany
    {
        return $this->hasMany(Kriteria::class);
    }
}
