<?php

namespace App\Http\Controllers;

use App\Models\Alternatif;
use App\Models\Kriteria;
use App\Models\PemetaanGap;
use App\Models\SubKriteria;
use Illuminate\Http\Request;

class AlternatifController extends Controller
{
    public function index()
    {
        $alternatif = Alternatif::query()->get();
        $kriteria = Kriteria::query()->get();
        return view('alternatif.index')
            ->with('kriteria', $kriteria)
            ->with('alternatif', $alternatif);
    }

    public function create()
    {
        $kriteria = Kriteria::query()->get();
        return view('alternatif.create')
            ->with('kriteria', $kriteria);
    }

    public function store(Request $request)
    {
        Alternatif::query()->update(['nilai_total' => null]);
        $alternatif = Alternatif::query()->create(['nama' => $request->get('nama')]);
        foreach ($request->get('kriteria') as $value) {
            $sub_kriteria = SubKriteria::query()
                ->where('id', $value)
                ->first();
            $alternatif->details()->create([
                'kriteria_id' => $sub_kriteria->kriteria_id,
                'sub_kriteria_id' => $value,
                'nilai' => $sub_kriteria->nilai
            ]);
        }
        return redirect()->route('alternatif.index');
    }

    public function edit(Alternatif $alternatif)
    {
        $kriteria = Kriteria::query()->get();
        return view('alternatif.edit')
            ->with('alternatif', $alternatif)
            ->with('kriteria', $kriteria);
    }

    public function update(Alternatif $alternatif, Request $request)
    {
        Alternatif::query()->update(['nilai_total' => null]);
        $alternatif->update(['nama' => $request->get('nama')]);
        foreach ($request->get('kriteria') as $value) {
            $sub_kriteria = SubKriteria::query()
                ->where('id', $value)
                ->first();
            $alternatif->details()->where([
                'kriteria_id' => $sub_kriteria->kriteria_id,
            ])->update([
                'sub_kriteria_id' => $value,
                'nilai' => $sub_kriteria->nilai
            ]);
        }
        return redirect()->route('alternatif.index');
    }

    public function destroy(Alternatif $alternatif)
    {
        $alternatif->delete();
        Alternatif::query()->update(['nilai_total' => null]);
        return redirect()->route('alternatif.index');
    }

    public function perhitunganAlternatif()
    {
        $alternatif = Alternatif::query()->get();
        $kriteria = Kriteria::query()->get();

        /*
        PERHITUNGAN TERKAIT KRITERIA PEMBERKASAN
        */
        $kode_pemberkasan = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'P10'];
        $aspekPemberkasan = Kriteria::query()->whereIn('kode', $kode_pemberkasan)->get();

        $nilaiAspekPemberkasan = [];
        foreach ($alternatif as $item) {
            $data = [
                'alternatif_id' => $item->id,
                'nama' => $item->nama
            ];
            foreach ($aspekPemberkasan as $key) {
                $detail = $item->details()->where('kriteria_id', $key->id)->first();
                $data[$key->kode] = $detail->nilai;
            }
            $nilaiAspekPemberkasan[] = $data;
        }

        // return $nilaiAspekPemberkasan;

        $tablePerhitunganGabPemberkasan = [];
        $nilaiGap = 4;
        foreach ($nilaiAspekPemberkasan as $key) {
            $data = [
                'alternatif_id' => $key['alternatif_id'],
                'nama' => $key['nama'],
                'P1' => $key['P1'] - $nilaiGap,
                'P2' => $key['P2'] - $nilaiGap,
                'P3' => $key['P3'] - $nilaiGap,
                'P4' => $key['P4'] - $nilaiGap,
                'P5' => $key['P5'] - $nilaiGap,
                'P6' => $key['P6'] - $nilaiGap,
                'P7' => $key['P7'] - $nilaiGap,
                'P8' => $key['P8'] - $nilaiGap,
                'P9' => $key['P9'] - $nilaiGap,
                'P10' => $key['P10'] - $nilaiGap,
            ];
            $tablePerhitunganGabPemberkasan[] = $data;
        }

        // return $tablePerhitunganGabPemberkasan;

        $tableBobotNilaiPemberkasan = [];
        foreach ($tablePerhitunganGabPemberkasan as $key) {
            $bobotP1 = PemetaanGap::where('gap', $key['P1'])->first()->bobot_nilai;
            $bobotP2 = PemetaanGap::where('gap', $key['P2'])->first()->bobot_nilai;
            $bobotP3 = PemetaanGap::where('gap', $key['P3'])->first()->bobot_nilai;
            $bobotP4 = PemetaanGap::where('gap', $key['P4'])->first()->bobot_nilai;
            $bobotP5 = PemetaanGap::where('gap', $key['P5'])->first()->bobot_nilai;
            $bobotP6 = PemetaanGap::where('gap', $key['P6'])->first()->bobot_nilai;
            $bobotP7 = PemetaanGap::where('gap', $key['P7'])->first()->bobot_nilai;
            $bobotP8 = PemetaanGap::where('gap', $key['P8'])->first()->bobot_nilai;
            $bobotP9 = PemetaanGap::where('gap', $key['P9'])->first()->bobot_nilai;
            $bobotP10 = PemetaanGap::where('gap', $key['P10'])->first()->bobot_nilai;
            $bobotCF = (($bobotP1 + $bobotP2 + $bobotP3) / 3);
            $bobotSF = (($bobotP4 + $bobotP5 + $bobotP6 + $bobotP7 + $bobotP8 + $bobotP9 + $bobotP10) / 7);
            $bobotNT = ($bobotCF * 0.6) + ($bobotSF * 0.4);
            $data = [
                'alternatif_id' => $key['alternatif_id'],
                'nama' => $key['nama'],
                'P1' => $bobotP1,
                'P2' => $bobotP2,
                'P3' => $bobotP3,
                'P4' => $bobotP4,
                'P5' => $bobotP5,
                'P6' => $bobotP6,
                'P7' => $bobotP7,
                'P8' => $bobotP8,
                'P9' => $bobotP9,
                'P10' => $bobotP10,
                'NCF' => $bobotCF,
                'NSF' => $bobotSF,
                'NT' => $bobotNT,
            ];
            $tableBobotNilaiPemberkasan[] = $data;

            Alternatif::where('id', $key['alternatif_id'])->update(['pemberkasan_ncf' => $bobotCF, 'pemberkasan_nsf' => $bobotSF, 'pemberkasan_nt' => $bobotNT]);
        }

        // return $tableBobotNilaiPemberkasan;


        /*
        PERHITUNGAN TERKAIT KRITERIA WAWANCARA
        */

        $aspekWawancara = Kriteria::query()->whereIn('kode', ['W1', 'W2', 'W3', 'W4', 'W5'])->get();

        $nilaiAspekWawancara = [];
        foreach ($alternatif as $item) {
            $data = [
                'alternatif_id' => $key['alternatif_id'],
                'nama' => $item->nama
            ];
            foreach ($aspekWawancara as $key) {
                $detail = $item->details()->where('kriteria_id', $key->id)->first();
                $data[$key->kode] = $detail->nilai;
            }
            $nilaiAspekWawancara[] = $data;
        }

        $tablePerhitunganGabWawancara = [];
        $nilaiGap = 5;
        foreach ($nilaiAspekWawancara as $key) {
            $data = [
                'alternatif_id' => $key['alternatif_id'],
                'nama' => $key['nama'],
                'W1' => $key['W1'] - $nilaiGap,
                'W2' => $key['W2'] - $nilaiGap,
                'W3' => $key['W3'] - $nilaiGap,
                'W4' => $key['W4'] - $nilaiGap,
                'W5' => $key['W5'] - $nilaiGap,
            ];
            $tablePerhitunganGabWawancara[] = $data;
        }

        $tableBobotNilaiWawancara = [];
        foreach ($tablePerhitunganGabWawancara as $key) {
            $bobotW1 = PemetaanGap::where('gap', $key['W1'])->first()->bobot_nilai;
            $bobotW2 = PemetaanGap::where('gap', $key['W2'])->first()->bobot_nilai;
            $bobotW3 = PemetaanGap::where('gap', $key['W3'])->first()->bobot_nilai;
            $bobotW4 = PemetaanGap::where('gap', $key['W4'])->first()->bobot_nilai;
            $bobotW5 = PemetaanGap::where('gap', $key['W5'])->first()->bobot_nilai;
            $bobotCF = (($bobotW1 + $bobotW2 + $bobotW3) / 3);
            $bobotSF = (($bobotW4 + $bobotW5) / 2);
            $bobotNT = ($bobotCF * 0.7) + ($bobotSF * 0.3);
            $data = [
                'alternatif_id' => $key['alternatif_id'],
                'nama' => $key['nama'],
                'W1' => $bobotW1,
                'W2' => $bobotW2,
                'W3' => $bobotW3,
                'W4' => $bobotW4,
                'W5' => $bobotW5,
                'NCF' => $bobotCF,
                'NSF' => $bobotSF,
                'NT' => $bobotNT,
            ];
            $tableBobotNilaiWawancara[] = $data;
        }

        // return $tableBobotNilaiWawancara;

        $pemberkasanWawancara = [];
        foreach ($alternatif as $key => $value) {
            $data = [
                'alternatif_id' => $value->id,
                'nama' => $value->nama,
                'pemberkasan' => $tableBobotNilaiPemberkasan[$key]['NT'],
                'wawancara' => $tableBobotNilaiWawancara[$key]['NT'],
            ];
            $pemberkasanWawancara[] = $data;
        }

        // return $pemberkasanWawancara;
        /*
        PERHTUNGAN SAW UNTUK BOBOT PEMBERKASAN DAN WAWANCARA
        */

        $dataHasilPM = collect($pemberkasanWawancara);
        $nilaiMinMax = [
            'min_pemberkasan' => $dataHasilPM->min('pemberkasan'),
            'max_pemberkasan' => $dataHasilPM->max('pemberkasan'),
            'bobot_pemberkasan' => 0.2,
            'min_wawancara' => $dataHasilPM->min('wawancara'),
            'max_wawancara' => $dataHasilPM->max('wawancara'),
            'bobot_wawancara' => 0.8,
        ];
        // return $nilaiMinMax;

        $tableNormalisasi = [];
        foreach ($pemberkasanWawancara as $key) {
            $data = [
                'alternatif_id' => $key['alternatif_id'],
                'nama' => $key['nama'],
                'saw_1' => $key['pemberkasan'] / $nilaiMinMax['max_pemberkasan'],
                'saw_2' => $key['wawancara'] / $nilaiMinMax['max_wawancara']
            ];
            $tableNormalisasi[] = $data;
        }
        // return $tableNormalisasi;

        $tableNilaiReferensi = [];
        foreach ($tableNormalisasi as $key) {
            $v1 = $key['saw_1'] * $nilaiMinMax['bobot_pemberkasan'];
            $v2 = $key['saw_2'] * $nilaiMinMax['bobot_wawancara'];
            $total = $v1 + $v2;
            $data = [
                'alternatif_id' => $key['alternatif_id'],
                'nama' => $key['nama'],
                'v_1' => $v1,
                'v_2' => $v2,
                'nilai_total' => $total
            ];
            $tableNilaiReferensi[] = $data;
            Alternatif::query()->where('id', $key['alternatif_id'])->update(['nilai_total' => $total]);
        }
        // return $tableNilaiReferensi;

        if (request()->get('redirect')) {
            return redirect()->route('alternatif.index');
        }

        return view('alternatif.perhitungan.index')
            ->with('kriteria', $kriteria)
            ->with('alternatif', $alternatif)
            ->with('nilaiAspekPemberkasan', $nilaiAspekPemberkasan)
            ->with('tablePerhitunganGabPemberkasan', $tablePerhitunganGabPemberkasan)
            ->with('tableBobotNilaiPemberkasan', $tableBobotNilaiPemberkasan)
            ->with('nilaiAspekWawancara', $nilaiAspekWawancara)
            ->with('tablePerhitunganGabWawancara', $tablePerhitunganGabWawancara)
            ->with('tableBobotNilaiWawancara', $tableBobotNilaiWawancara)
            ->with('pemberkasanWawancara', $pemberkasanWawancara)
            ->with('nilaiMinMax', $nilaiMinMax)
            ->with('tableNormalisasi', $tableNormalisasi)
            ->with('tableNilaiReferensi', $tableNilaiReferensi);
    }
}
