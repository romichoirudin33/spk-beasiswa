<?php

namespace App\Http\Controllers;

use App\Models\Alternatif;
use Illuminate\Http\Request;
use App\Models\Kriteria;

class WelcomeController extends Controller
{
    public function index()
    {
        $kriteria = Kriteria::query()->get();
        $rangking_pemberkasan = Alternatif::query()->orderBy('pemberkasan_nt', 'DESC')->get();
        $alternatif = Alternatif::query()->orderBy('nilai_total', 'DESC')->get();
        return view('welcome', compact('kriteria', 'alternatif', 'rangking_pemberkasan'));
    }

    public function rangking()
    {
        $alternatif = Alternatif::query()->orderBy('nilai_total', 'DESC')->get();
        return view('rangking', compact('alternatif'));
    }
}
