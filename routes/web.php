<?php

use App\Http\Controllers\AlternatifController;
use App\Http\Controllers\KriteriaController;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// return redirect()->route('home');
// });
Route::get('/', [WelcomeController::class, 'index']);
Route::get('rangking', [WelcomeController::class, 'rangking'])->name('rangking');

Auth::routes(['register' => false]);

Route::middleware('auth')->group(function () {
    Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('pengguna', PenggunaController::class, ['except' => ['show']]);

    Route::get('kriteria', [KriteriaController::class, 'index'])->name('kriteria.index');

    Route::get('alternatif', [AlternatifController::class, 'index'])->name('alternatif.index');
    Route::get('alternatif/create', [AlternatifController::class, 'create'])->name('alternatif.create');
    Route::post('alternatif', [AlternatifController::class, 'store'])->name('alternatif.store');
    Route::get('alternatif/{alternatif}/edit', [AlternatifController::class, 'edit'])->name('alternatif.edit');
    Route::put('alternatif/{alternatif}', [AlternatifController::class, 'update'])->name('alternatif.update');
    Route::delete('alternatif/{alternatif}', [AlternatifController::class, 'destroy'])->name('alternatif.destroy');
    Route::get('alternatif/perhitungan', [AlternatifController::class, 'perhitunganAlternatif'])->name('alternatif.perhitungan');
});
